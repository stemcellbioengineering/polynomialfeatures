numpy==1.19.2
scipy==1.5.2
pandas==1.1.4
setuptools==49.6.0.post20201009
scikit_learn==0.24.0
