#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 11:47:56 2020

@author: johnedgar
"""
import numbers
import numpy as np
from scipy import optimize

class clip_by_norm:
    '''
    Clips values that fall outside a given L2 norm

    Parameters
    ----------
    clip_norm : float
        L2 norm to clip to.

    Returns
    -------
    X_ : np.ndarray
        An array the same shape as X with values clipped

    '''
    def __init__(self, clip_norm):
        assert isinstance(clip_norm, numbers.Number)
        self.clip_norm = clip_norm
        
    def __call__(self, X):
        Xnorm = np.linalg.norm(X, axis = 1, keepdims = True)
        X_ = np.zeros(X.shape)
        X_ += (Xnorm <= self.clip_norm) * X
        X_ += (Xnorm > self.clip_norm) * self.clip_norm * X / Xnorm
        return X_

class clip_by_value:
    '''
    Clips values that fall outside a given range [clip_min, clip_max]

    Parameters
    ----------
    clip_min : float
        Minimum value to to clip to.
    clip_max : float
        Maximum value to to clip to.

    Returns
    -------
    X_ : np.ndarray
        An array the same shape as X with values clipped

    '''
    def __init__(self, clip_min, clip_max):
        assert isinstance(clip_min, numbers.Number)
        assert isinstance(clip_max, numbers.Number)
        self.clip_min = clip_min
        self.clip_max = clip_max
        
    def __call__(self, X):
        X_ = np.zeros(X.shape)
        X_ += ((X >= self.clip_min) * (X <= self.clip_max)) * X
        X_ += (X < self.clip_min) * self.clip_min
        X_ += (X > self.clip_max) * self.clip_max
        return X_
    
class clip_by_norm_mask:
    '''
    Creates a numpy mask for values that fall outside a given L2 norm.
    Norms greater than clip_norm are False while values within are True.

    Parameters
    ----------
    clip_norm : float
        Maximum allowable norm.
        
    Returns
    -------
    X_ : np.ndarray
        Boolean array of shape (n,1) where n is the rows in X.

    '''
    def __init__(self, clip_norm):
        assert isinstance(clip_norm, numbers.Number)
        self.clip_norm = clip_norm
        
    def __call__(self, X):
        Xnorm = np.linalg.norm(X, axis = 1, keepdims = True)
        X_ = (Xnorm <= self.clip_norm) 
        return X_

class clip_by_value_mask:
    '''
    Creates a numpy mask for values that fall outside a given
    range. Values less than clip_min or greater than clip_max are False
    while values within are True.

    Parameters
    ----------
    clip_min : float
        Minimum allowable value.
    clip_max : float
        Maximum allowable value.

    Returns
    -------
    X_ : np.ndarray
        Boolean array of shape (n,1) where n is the rows in X.

    '''
    def __init__(self, clip_min, clip_max):
        assert isinstance(clip_min, numbers.Number)
        assert isinstance(clip_max, numbers.Number)
        self.clip_min = clip_min
        self.clip_max = clip_max
        
    def __call__(self, X):
        X_ = np.prod(((X >= self.clip_min) * (X <= self.clip_max)), axis = 1).reshape((-1,1)) 
        return X_


class OptObjects:
    '''
    Optimization object that combines :class:`rsmprofiler.MultiRSM` and
    :class:`desirability.OverallDesirability` in a format that is
    compatible with scipy minimizers.

    Parameters
    ----------
    rsm_obj : :class:`rsmprofiler.MultiRSM` object
        The RSM object to be optimized.
    desr_obj : :class:`desirability.OverallDesirability` object
        The desirability object defining the optimization objectives.
    clip_min : float, optional
        The minimum value to clip to. The default is None (unconstrained).
    clip_max : float, optional
        The maximum value to clip to. The default is None (unconstrained).
    clip_norm : float, optional
        The maximum L2 norm to clip to. Ignored is clip_min and clip_max
        are provided. The default is None (unconstrained).
    sum_qty : bool, optional
        Sum the rows of X and include this value in the optimization. Can
        be used to minimize the total sum of X. An additional objective must
        be included in desr_obj when True. The default is False.

    '''
    def __init__(self, rsm_obj, desr_obj, clip_min = None, clip_max = None, clip_norm = None, sum_qty = False):
        # MultiRSM object
        self.rsm = rsm_obj
        # OverallDesirability object
        self.desr = desr_obj
        # Make clip mask object
        if clip_min is not None and clip_max is not None:
            self.clipmask = clip_by_value_mask(clip_min, clip_max)
        elif clip_norm is not None:
            self.clipmask = clip_by_norm_mask(clip_norm)
        elif clip_min is None and clip_max is None and clip_norm is None:
            self.clipmask = None
        else:
            raise ValueError('Must provide values to both clip_min and clip_max or provide a value to clip_norm or set all to None for no clipping')
            
        self.sum_qty = sum_qty
        
    def __call__(self, X):
        if len(X.shape) == 1:
            X = X.reshape((1,-1))

        # Get RSM response Y
        Y = self.rsm(X)
        
        # Sums values by row 
        if self.sum_qty:
            Xsum = np.sum(self.rsm.inverse_scale_factors(X), axis = 1, keepdims = True)
            Y = np.hstack([Y,Xsum])
    
        if self.clipmask is not None:
            return 1. - self.desr(Y) * self.clipmask(X)
        else:
            return 1. - self.desr(Y)
        
    
def neldermead(opt_obj, n_starts = 10, maxiter = 1000, x_range = None, x_norm = None, verbose = False):
    '''
    Nelder-Mead optimization algorithm

    Parameters
    ----------
    opt_obj : :class:`rsmoptimize.OptObject` object
        Optimizer object containing the RSMs and desirability functions.
        DESCRIPTION.
    n_starts : int, optional
        Number of starts from random initializations. The default is 10.
    maxiter : int, optional
        Maximum number of algorithm iterations. The default is 1000.
    x_range : list, tuple, optional
        The range [min, max] for initializations. The default is [-1,1].
    x_norm : float, optional
        The maximum L2 norm for initializations. Ignored if x_range is provided.
        The default is None.
    verbose : bool, optional
        Print optimization updates. The default is False.

    Returns
    -------
    resX : np.ndarray
        Array containing n_starts most optimum solutions.
    resD : np.ndarray
        Array containing n_starts highest desirabilities.

    '''
    n_dim = opt_obj.rsm.n_factors    
    if x_range is None and x_norm is None:
        X0 = 2 * np.random.random((n_starts, n_dim)) - 1
    elif isinstance(x_range, (list, tuple)):
        X0 = (x_range[1]-x_range[0]) * np.random.random((n_starts, n_dim)) - (x_range[1]-x_range[0])/2
    elif isinstance(x_norm, numbers.Number):
        clip_norm = clip_by_norm(x_norm)
        X0 = clip_norm(2 * x_norm * np.random.random((n_starts, n_dim)) - x_norm)
    else:
        raise ValueError('xconstraint is not valid.')
    
    
    resX = []
    for idx in range(X0.shape[0]):
        res = optimize.minimize(opt_obj, X0[idx], method='nelder-mead',
                                options={'maxiter': maxiter,'xatol': 1e-8, 'disp': verbose})
        resX.append(res.x)
     
    resX = np.vstack(resX)
    resD = 1. - opt_obj(resX)
    return resX, resD

def basinhopping(opt_obj, n_starts = 3, maxiter = 200, method = 'nelder-mead', x_range = None, x_norm = None, verbose = False):
    '''
    Basinhopping optimization algorithm

    Parameters
    ----------
    opt_obj : :class:`rsmoptimize.OptObject` object
        Optimizer object containing the RSMs and desirability functions.
        DESCRIPTION.
    n_starts : int, optional
        Number of starts from random initializations. The default is 3.
    maxiter : int, optional
        Maximum number of algorithm iterations. The default is 200.
    method : str, optional
        The optimizer subroutine used by basinhopping. One of
        :class:`scipy.optimize.minimize` methods. The default is 'nelder-mead'.
    x_range : list, tuple, optional
        The range [min, max] for initializations. The default is [-1,1].
    x_norm : float, optional
        The maximum L2 norm for initializations. Ignored if x_range is provided.
        The default is None.
    verbose : bool, optional
        Print optimization updates. The default is False.


    Returns
    -------
    resX : np.ndarray
        Array containing n_starts most optimum solutions.
    resD : np.ndarray
        Array containing n_starts highest desirabilities.

    '''
    n_dim = opt_obj.rsm.n_factors    
    if x_range is None and x_norm is None:
        X0 = 2 * np.random.random((n_starts, n_dim)) - 1
        xlower = -1.
        xupper = 1.
    elif isinstance(x_range, (list, tuple)):
        xupper = x_range[1]
        xlower = x_range[0]
        X0 = (xupper-xlower) * np.random.random((n_starts, n_dim)) - (xupper-xlower)/2
        
    elif isinstance(x_norm, numbers.Number):
        clip_norm = clip_by_norm(x_norm)
        X0 = clip_norm(2 * x_norm * np.random.random((n_starts, n_dim)) - x_norm)
        xupper = x_norm
        xlower = -x_norm
    else:
        raise ValueError('xconstraint is not valid.')
    
    resX = []
    for idx in range(X0.shape[0]):
        res = optimize.basinhopping(opt_obj, X0[idx], niter = maxiter,
                        minimizer_kwargs = {'method': method}, disp=verbose)
        resX.append(res.x)
     
    resX = np.vstack(resX)
    resD = 1. - opt_obj(resX)
    return resX, resD


        
            
            
        
        
        
        
        