#    @author: johnedgar
#
from setuptools import setup

def readme():
    with open('README.md') as readme:
        return readme.read()

setup(name='polynomialfeatures',
      version='1.1',
      description='A small library for building polynomial models from regression coefficients',
      long_descriptions=readme(),
      classifiers=[
                   'Development Status :: 3 - Alpha',
                   'License :: OSI Approved :: MIT License',
                   'Programming Language :: Python :: 3.7',
                   'Topic :: Scientific/Engineering'
                   ],
      url='https://gitlab.com/stemcellbioengineering/polynomialfeatures',
      author='John Edgar',
      author_email='edgarjohnmichael@gmail.com',
      license='MIT',
      packages=['polynomialfeatures'],
      install_requires=['numpy','scipy','pandas','scikit_learn'],
      include_package_data=True,
      zip_safe=False)



